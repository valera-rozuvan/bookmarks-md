# bookmarks-md

a collection of bookmarks organized in Markdown files

## toc

- [BlockChain](blockchain/)
- [Electronics, hardware, robotics](electronics-hardware-robotics/)
- [Photography](photography/)
- [Security](security/)
- [Computer Science](computer-science/)
- [Docker](docker/)
- [DevOps](devops/)
- [Static site/blog generators](static-site-blog-generators/)
- [JavaScript](javascript/)
- [Useful utilities](useful-utilities/)
- [General programming articles](general-programming-articles/)
- [MS Windows stuff](ms-windows-stuff/)
- [Marketing](marketing/)
- [Music theory](music-theory/)
- [Android](android/)
- [Mathematics](mathematics/)
- [Interesting books, short stories, etc.](interesting-books-short-stories-etc/)
- [Markdown](markdown/)
- [Stockfish (chess)](https://en.wikipedia.org/wiki/Stockfish_(chess))

---

## license

The project `'bookmarks-md'` is licensed under the CC0-1.0.

See [LICENSE](./LICENSE) for more details.

The latest source code can be retrieved from one of several mirrors:

1. [github.com/valera-rozuvan/bookmarks-md](https://github.com/valera-rozuvan/bookmarks-md)

2. [gitlab.com/valera-rozuvan/bookmarks-md](https://gitlab.com/valera-rozuvan/bookmarks-md)

3. [git.rozuvan.net/bookmarks-md](https://git.rozuvan.net/bookmarks-md)

Copyright (c) 2015-2022 [Valera Rozuvan](https://valera.rozuvan.net/)
